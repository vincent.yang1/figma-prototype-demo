module.exports = {
  root: true,
  extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended'],
  plugins: ['workspaces','@typescript-eslint'],
  rules: {
    'workspaces/no-relative-imports': 'error',
    'workspaces/require-dependency': 'error',
  },
  parser: '@typescript-eslint/parser',
  "parserOptions": {
      "sourceType": "module",
      "ecmaVersion": 2020
  }
}