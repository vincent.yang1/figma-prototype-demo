import React, {useEffect} from 'react';
import {Image} from 'react-native';
import {log} from '@sdcx/common';

export function Flower() {
  useEffect(() => {
    log('渲染了 Flower ');
  });

  return <Image source={require('./images/flower_1.png')} />;
}
