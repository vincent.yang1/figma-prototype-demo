import Animated, {
  FadeOut,
  Easing,
  FadeIn,
  Layout,
} from 'react-native-reanimated';
import { TouchableOpacity,View, Text } from 'react-native';

import React, {useState} from 'react';

export function Day({type, num}) {
  const [status, setStatus] = useState(type)

  return (
    <>

      {status === 'disabled' && (
        <View className="bg-white flex flex-col justify-center items-center w-8 h-8"> 
          <Text className="text-left align-top text-xs font-Roboto font-null whitespace-nowrap text-[#00000049]"> 
          {num} 
          </Text> 
        </View>

      )}

      {status === 'normal' && (
        <Animated.View
          className='h-8 w-8'
          entering={FadeIn.duration(1500).easing(Easing.bezier(1, 0.01, 0, 1))}
          exiting={FadeOut.duration(1500).easing(Easing.inOut(Easing.ease))}
        >
          <TouchableOpacity className='h-8 w-8' onPress={() => setStatus("active")} >
            <View className="bg-white flex flex-col justify-center items-center w-8 h-8"> 
              <Text className="text-left align-top text-xs font-Roboto font-null whitespace-nowrap text-black"> 
              {num} 
              </Text> 
            </View>
          </TouchableOpacity>
        </Animated.View>

      )}

      {
      status === 'active' && (
        <Animated.View
          className='h-8 w-8'
          entering={FadeIn.duration(1500).easing(Easing.in(Easing.exp))}
          exiting={FadeOut.duration(1500).easing(Easing.bezierFn(0.25, 0.1, 0.25, 1))}
        >
          <TouchableOpacity className='h-8 w-8' onPress={() => setStatus("normal")} >
            <View className="bg-[#694ce2] rounded-full  flex flex-col justify-center items-center w-8 h-8"> 
              <Text className="text-left align-top text-xs font-Roboto font-bold whitespace-nowrap text-white"> 
              {num}
              </Text> 
            </View>
          </TouchableOpacity>
         </Animated.View>
      )}

    </>
  );
}
const days = 
        [
          [-29, -30, -31, 1, 2, 3, 4],
          [5, 6, 7, 8, 9, 10, 11],
          [12, 13, 14, 15, 16, 17, 18],
          [19, 20, 21, 22, 23, 24, 25],
          [26, 27, 28, 29, 30, -1, -2],
          [-3, -4, -5, -6, -7, -8, -9]
        ]
export function Calendar() {

  return (
    <View className="flex flex-row rounded-lg bg-white p-3">
      <View className="flex h-56 w-56 flex-col">
        <View className="flex h-8 w-56 flex-row">
          {
            ['Mo','Tu','We','Th','Fri','Sa','Su'].map(item=>
            <View className="flex h-8 w-8 flex-col items-center justify-center bg-white">
              <Text className="font-Roboto whitespace-nowrap text-left align-top text-xs font-bold text-black"> {item} </Text>
            </View>)
          }

        </View>
        {
          days.map(item=>{
            return (
              <View className="flex h-8 w-56 flex-row">
                {item.map(subItem=>{
                  return <Day type={subItem < 0 ? 'disabled' : 'normal'} num={Math.abs(subItem)}/>
                })}
              </View>
            )
          })
        }


      </View>


    </View>
  );
}

