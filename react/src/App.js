
import React, { useState } from "react";

import './App.css';
import Day from './components/Day'
import Overlay from './components/Overlay'
const days = 
        [
          [-29, -30, -31, 1, 2, 3, 4],
          [5, 6, 7, 8, 9, 10, 11],
          [12, 13, 14, 15, 16, 17, 18],
          [19, 20, 21, 22, 23, 24, 25],
          [26, 27, 28, 29, 30, -1, -2],
          [-3, -4, -5, -6, -7, -8, -9]
        ]
function App() {

  return (
    <div className="flex flex-row rounded-lg bg-white p-3">
      <div className="flex h-56 w-56 flex-col">
        <div className="flex h-8 w-56 flex-row">
          {
            ['Mo','Tu','We','Th','Fri','Sa','Su'].map(item=>
              <div className="flex h-8 w-8 flex-col items-center justify-center bg-white px-3 py-0.5">
                <span className="font-Roboto whitespace-nowrap text-left align-top text-xs font-bold text-black"> {item} </span>
              </div>)
          }
        </div>
        {
          days.map(item=>{
            return (
              <div className="flex h-8 w-56 flex-row">
                {item.map(subItem=>{
                  return <Day type={subItem < 0 ? 'disabled' : 'normal'} num={Math.abs(subItem)}/>
                })}
              </div>
            )
          })
        }


      </div>


      <Overlay/>
    </div>
  );
}

export default App;
