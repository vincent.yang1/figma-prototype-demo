import React, { useState } from 'react'
import QueueAnim from "rc-queue-anim";

function Overlay() {
  const [showOverlay, toggleOverlay] = useState(false);
  const [animate, setAnimate] = useState({ease:"linear",duration:1000})

  return (
    <div>
      <button onClick={() => toggleOverlay(true)}>Show overlay</button>
      <QueueAnim
        key={'asdas'}
        type={"alpha"}
        duration={[animate.duration]}
        ease={[animate.ease]}
      >
        {showOverlay ? (
          <div  key="a" className="overlayBG" onClick={() => toggleOverlay(false)}>
            <div
              style={{ position: "relative", width: "100%", height: "100%" }}
            >
              <div
                className="overlayContent"
                onClick={(e) => {
                  e.stopPropagation();
                }}
              >
                Hello there
              </div>
            </div>
          </div>
        ): null}

      </QueueAnim>
    </div>
  );
}
class Test extends React.Component{
  state = {
    show: true
  };
  onClick = () => {
    this.setState({
      show: !this.state.show
    });
  }
  render() {
    return (
      <div className="queue-demo">
        <p className="buttons">
          <button type="primary" onClick={this.onClick}>Switch</button>
        </p>
        <QueueAnim className="demo-content"
          key="demo"
          type={['right', 'left']}
          ease={['easeOutQuart', 'easeInOutQuart']}>
          {this.state.show ? [
            <div className="demo-thead" key="a">
          <div className="overlayBG" >
            <div
              style={{ position: "relative", width: "100%", height: "100%" }}
            >
              <div
                className="overlayContent"
                onClick={(e) => {
                  e.stopPropagation();
                }}
              >
                Hello there
              </div>
            </div>
          </div>
            </div>

          ] : null}
        </QueueAnim>
      </div>
    );
  }
};

export default Overlay;

