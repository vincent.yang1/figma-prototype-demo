import { useState } from 'react'
import QueueAnim from "rc-queue-anim";

function Day({type, num}) {
  const [status, setStatus] = useState(type)
  const [animate, setAnimate] = useState({ease:"linear",duration:1})

  return (
    <div  className="h-8 w-8">
      <QueueAnim
        key={status}
        type={"alpha"}
        duration={[animate?.duration]}
        ease={[animate?.ease]}
      >
              {status === "disabled" ? (
                <div key="disabled" >
                  <div className="flex h-8 w-8 flex-col items-center justify-center bg-white px-3 py-0.5 cursor-not-allowed">
                    <span className="font-Roboto font-null whitespace-nowrap text-left align-top text-xs text-[#00000049]">
                      {num}
                    </span>
                  </div>
                </div>
              ) : null}
              {status === "normal" ? (
                <div 
                  key="a"
                  // onMouseEnter={() => {
                  //   setStatus("hovering")
                  //   setAnimate({
                  //     ease: "linear",
                  //     duration: 300
                  //   })
                  // }} 
                  onClick={() => {
                    setStatus("active")
                    setAnimate({
                      ease: "linear",
                      duration: 300
                    })
                  }} 

                  >
                  <div className="flex h-8 w-8 flex-col items-center justify-center bg-white px-3 py-0.5 cursor-pointer" >
                    <span className="font-Roboto font-null whitespace-nowrap text-left align-top text-xs text-black">
                      {num}
                    </span>
                  </div>
                </div>
              ) : null}
              {status === "hovering" ? (
                <div key="a"

                  // onMouseLeave={() => {
                  //   setStatus("normal")
                  //   setAnimate({
                  //     ease: "linear",
                  //     duration: 300
                  //   })
                  // }}
                  >
                  <div className="flex h-8 w-8 flex-col items-center justify-center rounded-3xl bg-[#f0f0f0] px-3 py-0.5 cursor-pointer" >
                    <span className="font-Roboto whitespace-nowrap text-left align-top text-xs font-bold text-[#694de0]">
                      {num}
                    </span>
                  </div>
                </div>
              ) : null}

              {status === "active" ? (
                <div 
                  key="a" 
                  onClick={() => {
                    setStatus("normal")
                    setAnimate({
                      ease: "linear",
                      duration: 300
                    })
                  }}
                >
                  <div className="flex h-8 w-8 flex-col items-center justify-center rounded-3xl bg-[#694ce2] px-3 py-0.5 cursor-pointer">
                    <span className="font-Roboto whitespace-nowrap text-left align-top text-xs font-bold text-white">
                      {num}
                    </span>
                  </div>
                </div>
              ) : null}

      </QueueAnim>
    </div>
  );
}

export default Day;
